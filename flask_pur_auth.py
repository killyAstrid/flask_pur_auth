import requests
import jwt

from flask import request, abort, current_app as app

public_keys = []

def jwt_auth(f):
    def decorator(*args, **kwargs):
        auth = dict(request.headers).get('Authorization', None)
        payload = verify_token(auth)
        if not payload:
            abort(401)
        kwargs['payload'] = payload
        return f(*args, **kwargs)
    return decorator

def verify_token(auth_header):
    if not auth_header:
        return None
    token_type, token = auth_header.split(' ')
    if not token_type or not token:
        return None
    # first try with current known keys
    payload = get_payload(token)
    if payload:
        return payload
    # failed with current keys
    update_keys()
    return get_payload(token)

def get_payload(token):
    for key in public_keys:
        try:
            payload = jwt.decode(token, key, algorithms=['RS256'])
            return payload
        except Exception as e:
            app.logger.error(e)
            pass
    return None

def update_keys():
    global public_keys
    r = requests.get(
        '{}://{}:{}/{}'.format(
            app.config['PROTOC_JWT_PROVIDER'],
            app.config['JWT_PROVIDER_HOST'],
            app.config['JWT_PROVIDER_PORT'],
            app.config['JWT_PROVIDER_URL']
        )
    )
    public_keys = [key.encode() for key in r.json()['pubs']]
