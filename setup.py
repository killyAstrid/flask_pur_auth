
from setuptools import setup

setup(
    name='Flask-PUR-Auth',
    version='1.0',
    author='Abel Stam',
    author_email='abelstam@hotmail.com',
    description='Very short description',
    long_description=__doc__,
    py_modules=['flask_pur_auth'],
    zip_safe=False,
    include_package_data=True,
    platforms='any',
    install_requires=[
        'Flask>=1',
        'PyJWT>=1.7.1',
        'requests>=2'
    ],
    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ]
)