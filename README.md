# Authenticate asymetric jwt
Validate jwts with public keys from your auth server.

# How it works
Auth server issues a token signed with private key (RSA). The auth server can expose all valid public keys corresponding to the private keys. As they cannot be used to generate new tokens, this will not lead to un-auth access. However, anyone with the publickeys can view the content of jwts. Be carefull what kind of data you put in your jwt payload and to what network you expose the pub keys. Put the jwt_pur decorator around the view you want to protect. The decorator will check the incoming requests with the known keys. If this fails it will pick up the valid keys with the jwt auth service and try again.

# Please note
A lot of invalid requests send to the decorator will result in a lot of http requests to the auth server. 
The decorator is not yet tested for highly active multi-threaded applications. Race conditions might occur.

